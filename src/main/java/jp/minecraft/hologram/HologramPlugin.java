package jp.minecraft.hologram;

import jp.commun.minecraft.util.command.CommandManager;
import jp.commun.minecraft.util.command.CommandPermissionException;
import jp.minecraft.hologram.command.PluginCommand;
import jp.minecraft.hologram.listener.PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

/**
 * User: ayu
 * Date: 14/02/25
 * Time: 22:15
 */
public class HologramPlugin extends JavaPlugin {
    private static HologramPlugin plugin;
    private CommandManager commandManager;
    private HologramManager hologramManager;

    @Override
    public void onEnable() {
        super.onEnable();

        plugin = this;

        loadConfiguration();

        commandManager = new CommandManager();
        commandManager.register(new PluginCommand(this));
        hologramManager = new HologramManager(this);
        hologramManager.load();

        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new PlayerListener(this), this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        try {
            commandManager.execute(sender, command, args);
        } catch (CommandPermissionException e) {
            sender.sendMessage(ChatColor.RED + "You don't have permission to do this.");
        } catch (CommandException e) {
            sender.sendMessage(e.getMessage());
        }

        return false;
    }

    public void loadConfiguration() {
        FileConfiguration config = getConfig();
        config.options().copyDefaults(true);
        saveConfig();
    }

    public HologramManager getHologramManager() {
        return hologramManager;
    }

    public static HologramPlugin getPlugin() {
        return plugin;
    }
}
