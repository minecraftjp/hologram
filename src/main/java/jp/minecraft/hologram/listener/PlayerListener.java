package jp.minecraft.hologram.listener;

import jp.minecraft.hologram.Hologram;
import jp.minecraft.hologram.HologramPlugin;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * User: ayu
 * Date: 14/02/26
 * Time: 22:16
 */
public class PlayerListener implements Listener {
    private final HologramPlugin plugin;

    public PlayerListener(HologramPlugin plugin) {
        this.plugin = plugin;
    }


    /*
    @EventHandler(ignoreCancelled = true)
    public void onPlayerMove(PlayerMoveEvent event) {
        for (Hologram hologram: plugin.getHologramManager().getHolograms()) {
            hologram.move(event.getPlayer(), event.getTo());
        }
    }*/

    @EventHandler(ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent event) {
        final Player player = event.getPlayer();

        for (Hologram hologram: plugin.getHologramManager().getHolograms()) {
            hologram.show(player);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerWorld(PlayerChangedWorldEvent event) {
        for (Hologram hologram: plugin.getHologramManager().getHolograms()) {
            hologram.show(event.getPlayer());
        }
    }
}
