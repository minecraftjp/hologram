package jp.minecraft.hologram.command;

import jp.commun.minecraft.util.command.Command;
import jp.commun.minecraft.util.command.CommandHandler;
import jp.minecraft.hologram.HologramPlugin;
import org.bukkit.command.CommandSender;

/**
 * User: ayu
 * Date: 14/02/26
 * Time: 22:18
 */
public class PluginCommand implements CommandHandler {
    private final HologramPlugin plugin;

    public PluginCommand(HologramPlugin plugin) {
        this.plugin = plugin;
    }

    @Command( names = { "holo reload" }, permissions = { "hologram.reload" })
    public void reload(CommandSender sender, String commandName, String[] args) {
        plugin.reloadConfig();
        plugin.getHologramManager().load();

        sender.sendMessage("[Hologram] reloaded.");
    }
}
