package jp.minecraft.hologram.util;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/**
 * User: ayu
 * Date: 13/11/04
 * Time: 12:29
 */
public class Packet extends Object{

    private Object craftedPacket = null;

    public Packet(String name){
        try {
            craftedPacket = ReflectionUtil.getNMSClass(name);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setPublicValue(String field, Object value){
        try {
            Field f = craftedPacket.getClass().getField(field);
            f.setAccessible(true);
            f.set(craftedPacket, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setPrivateValue(String field, Object value){
        try {
            Field f = craftedPacket.getClass().getDeclaredField(field);
            f.setAccessible(true);
            f.set(craftedPacket, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void send(Player player){
        try {
            Object entityPlayer = ReflectionUtil.getMethod("getHandle", player.getClass(), 0).invoke(player);
            Object playerConnection = entityPlayer.getClass().getField("playerConnection").get(entityPlayer);
            ReflectionUtil.getMethod("sendPacket", playerConnection.getClass(), 1).invoke(playerConnection, craftedPacket);
        } catch (Exception e) {
            Bukkit.getLogger().warning("[PacketUtil] Failed to send packet to " + player.getName() + "!");
        }
    }

    public String toString() {
        try {
            return (String) ReflectionUtil.getMethod("b", craftedPacket.getClass(), 0).invoke(craftedPacket);
        } catch (IllegalAccessException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InvocationTargetException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return "";
    }

    public Object getCraftedPacket() {
        return craftedPacket;
    }
}