package jp.minecraft.hologram.util;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * User: ayu
 * Date: 13/11/04
 * Time: 12:30
 */
public class ReflectionUtil {
    public static Object getNMSClass(String name, Object... args) throws Exception {
        Class<?> c = Class.forName(ReflectionUtil.getPackageName() + "." + name);
        int params = 0;
        if (args != null) {
            params = args.length;
        }
        for (Constructor<?> co : c.getConstructors()) {
            if (co.getParameterTypes().length == params) {
                return co.newInstance(args);
            }
        }
        return null;
    }

    public static Object getNMSClass(String name, int len, Object... args) throws Exception {
        Class<?> c = Class.forName(ReflectionUtil.getPackageName() + "." + name);
        if (args == null && len > 0) args = new Object[]{ null };
        for (Constructor<?> co : c.getConstructors()) {
            if (co.getParameterTypes().length == len) {
                return co.newInstance(args);
            }
        }
        return null;
    }

    public static Object getNMSClassExact(String name, Class[] types, Object... args) throws Exception {
        Class<?> c = Class.forName(ReflectionUtil.getPackageName() + "." + name);
        Constructor<?> co = c.getConstructor(types);
        return co.newInstance(args);
    }

    public static Class<?> getCraftClassExact(String name, Object... args) {
        Class<?> c;
        try {
            c = Class.forName(ReflectionUtil.getCraftPackageName() + "." + name);
        } catch(Exception e){
            return null;
        }
        return c;
    }

    public static Method getMethod(String name, Class<?> c, Class<?>[] args) {
        for (Method m : c.getMethods()) {
            if(m.getName().equals(name) && ClassListEqual(args, m.getParameterTypes())) {
                return m;
            }
        }
        return null;
    }

    public static Method getMethod(String name, Class<?> c, int params) {
        for (Method m : c.getMethods()) {
            if (m.getName().equals(name) && m.getParameterTypes().length == params) {
                return m;
            }
        }
        return null;
    }

    public static Method getMethod(String name, Class<?> c) {
        for (Method m : c.getMethods()) {
            if (m.getName().equals(name)) {
                return m;
            }
        }
        return null;
    }

    public static Field getField(String name, Class<?> c) {
        for (Field f : c.getFields()) {
            if (f.getName().equals(name)) {
                return f;
            }
        }
        return null;
    }

    public static void setValue(Object instance, String fieldName, Object value) throws Exception {
        Field field = instance.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(instance, value);
    }

    public static String getCraftPackageName() {
        return "org.bukkit.craftbukkit." + Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
    }

    public static String getPackageName() {
        return "net.minecraft.server." + Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
    }

    public static Object BukkitPlayerToEntityPlayer(Player player){
        Object entityPlayer = null;
        try{
            entityPlayer = ReflectionUtil.getMethod("getHandle", player.getClass(), 0).invoke(player);
        }catch(Exception e){
            return null;
        }
        return entityPlayer;
    }

    public static boolean ClassListEqual(Class<?>[] l1, Class<?>[] l2){
        boolean equal = true;

        if(l1.length != l2.length)return false;
        for(int i=0; i<l1.length; i++){
            if(l1[i] != l2[i]){equal=false;break;}
        }

        return equal;
    }
}
