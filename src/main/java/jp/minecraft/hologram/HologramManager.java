package jp.minecraft.hologram;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * User: ayu
 * Date: 14/02/26
 * Time: 22:01
 */
public class HologramManager {
    private final HologramPlugin plugin;
    private final List<Hologram> holograms = new ArrayList<Hologram>();

    public HologramManager(HologramPlugin plugin) {
        this.plugin = plugin;
    }

    public void load() {
        hideAll();
        holograms.clear();

        for (Map<?, ?> map: plugin.getConfig().getMapList("holograms")) {
            if (!map.containsKey("location") || !map.containsKey("lines")) continue;

            String locationStr = (String)map.get("location");
            String[] parts = locationStr.split(",");
            World world = plugin.getServer().getWorld(parts[0]);
            if (world == null) continue;

            double x = Double.parseDouble(parts[1]);
            double y = Double.parseDouble(parts[2]);
            double z = Double.parseDouble(parts[3]);

            Location location = new Location(world, x, y, z);
            List<String> lines = (List<String>)map.get("lines");

            holograms.add(new Hologram(location, lines));
        }
        showAll();
    }

    public void hideAll() {
        for (Player player: plugin.getServer().getOnlinePlayers()) {
            for (Hologram hologram: holograms) {
                hologram.hide(player);
            }
        }
    }

    public void showAll() {
        for (Player player: plugin.getServer().getOnlinePlayers()) {
            for (Hologram hologram: holograms) {
                hologram.show(player);
            }
        }
    }

    public List<Hologram> getHolograms() {
        return holograms;
    }
}
