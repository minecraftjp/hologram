package jp.minecraft.hologram;

import jp.minecraft.hologram.util.Packet;
import jp.minecraft.hologram.util.ReflectionUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.lang.reflect.Method;
import java.util.List;

/**
 * User: ayu
 * Date: 14/02/25
 * Time: 22:36
 */
public class Hologram {
    private static int BASE_ENTITY_ID = -1900000;
    private final int entityId;
    private Location location;
    private List<String> lines;
    private boolean absolute = true;
    private String version;

    public Hologram(Location location, List<String> lines) {
        this.entityId = BASE_ENTITY_ID;
        BASE_ENTITY_ID += lines.size() * 2;
        this.location = location;
        this.lines = lines;

        this.version = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
    }

    public Hologram(Location location, List<String> lines, boolean absolute) {
        this(location, lines);
        this.absolute = absolute;
    }

    public void show(Player player) {
        if (!location.getWorld().equals(player.getWorld())) return;
        for (int i = 0; i < lines.size(); i++) {
            String message = lines.get(i);
            Packet mobPacket = getMobPacket(i, formatMessage(message, player));
            Packet skullPacket = getSkullPacket(i);
            Packet attachPacket = getAttachPacket(i);

            mobPacket.send(player);
            skullPacket.send(player);
            attachPacket.send(player);
        }
    }

    public void move(Player player, Location location) {
        if (!location.getWorld().equals(player.getWorld()) || absolute) return;
/*
        location.add(this.location);
        for (int i = 0; i < lines.size(); i++) {
            Packet packet = getMovePacket(i, entityId + 2 * i, location);
            packet.send(player);

            packet = getMovePacket(i, entityId + 1 + 2 * i, location);
            packet.send(player);
        }
*/
    }

    public void hide(Player player) {
        if (!location.getWorld().equals(player.getWorld())) return;

        int[] ids = new int[lines.size() * 2];
        for (int i = 0; i < lines.size(); i++) {
            ids[i * 2] = entityId + 2 * i;
            ids[i * 2 + 1] = entityId + 1 + 2 * i;
        }

        Packet packet;
        if (version.startsWith("v1_6")) {
            packet = new Packet("Packet29DestroyEntity");
            packet.setPublicValue("a", ids);
        } else {
            packet = new Packet("PacketPlayOutEntityDestroy");
            packet.setPrivateValue("a", ids);
        }

        packet.send(player);
    }

    public Packet getMobPacket(int line, String message) {
        Packet packet;
        if (version.startsWith("v1_6")) {
            packet = new Packet("Packet24MobSpawn");
            packet.setPublicValue("a", entityId + 2 * line);
            packet.setPublicValue("b", EntityType.HORSE.getTypeId());
            packet.setPublicValue("c", (int)Math.floor(location.getX() * 32.0D)); // X
            packet.setPublicValue("d", (int)Math.floor(((location.getY() - 0.25D * line) + 55D) * 32.0D)); // Y
            packet.setPublicValue("e", (int)Math.floor(location.getZ() * 32.0D)); // Z
            packet.setPublicValue("f", 0); // Pitch
            packet.setPublicValue("g", 0); // Head Pitch
            packet.setPublicValue("h", 0); // Yaw
            packet.setPublicValue("i", (byte)0); // X Velocity
            packet.setPublicValue("j", (byte)0); // Y Velocity
            packet.setPublicValue("k", (byte)0); // Z Velocity

            try {
                packet.setPrivateValue("t", getWatcher(message));
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        } else {
            packet = new Packet("PacketPlayOutSpawnEntityLiving");
            packet.setPrivateValue("a", entityId + 2 * line);
            packet.setPrivateValue("b", EntityType.HORSE.getTypeId());
            packet.setPrivateValue("c", (int) Math.floor(location.getX() * 32.0D)); // X
            packet.setPrivateValue("d", (int) Math.floor(((location.getY() - 0.25D * (double)line) + 55D) * 32.0D)); // Y
            packet.setPrivateValue("e", (int) Math.floor(location.getZ() * 32.0D)); // Z
            packet.setPrivateValue("f", 0); // Pitch
            packet.setPrivateValue("g", 0); // Head Pitch
            packet.setPrivateValue("h", 0); // Yaw
            packet.setPrivateValue("i", (byte) 0); // X Velocity
            packet.setPrivateValue("j", (byte) 0); // Y Velocity
            packet.setPrivateValue("k", (byte) 0); // Z Velocity

            try {
                packet.setPrivateValue("l", getWatcher(message));
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }


        return packet;
    }

    public Packet getSkullPacket(int line) {
        Packet packet;
        if (version.startsWith("v1_6")) {
            packet = new Packet("Packet23VehicleSpawn");
            packet.setPublicValue("a", entityId + 1 + 2 * line);
            packet.setPublicValue("b", (int)Math.floor(location.getX() * 32.0D)); // X
            packet.setPublicValue("c", (int)Math.floor(((location.getY() - 0.25D * (double)line) + 55D) * 32.0D)); // Y
            packet.setPublicValue("d", (int)Math.floor(location.getZ() * 32.0D)); // Z
            packet.setPublicValue("e", (byte)0); // X Velocity
            packet.setPublicValue("f", (byte)0); // Y Velocity
            packet.setPublicValue("g", (byte)0); // Z Velocity
            packet.setPublicValue("h", 0); // Pitch
            packet.setPublicValue("i", 0); // Yaw
            packet.setPublicValue("j", 66); // type
            packet.setPublicValue("k", 0); // ?
        } else {
            packet = new Packet("PacketPlayOutSpawnEntity");
            packet.setPrivateValue("a", entityId + 1 + 2 * line);
            packet.setPrivateValue("b", (int) Math.floor(location.getX() * 32.0D)); // X
            packet.setPrivateValue("c", (int) Math.floor(((location.getY() - 0.25D * (double)line) + 55D) * 32.0D)); // Y
            packet.setPrivateValue("d", (int) Math.floor(location.getZ() * 32.0D)); // Z
            packet.setPrivateValue("e", (byte) 0); // X Velocity
            packet.setPrivateValue("f", (byte) 0); // Y Velocity
            packet.setPrivateValue("g", (byte) 0); // Z Velocity
            packet.setPrivateValue("h", 0); // Pitch
            packet.setPrivateValue("i", 0); // Yaw
            packet.setPrivateValue("j", 66); // type
            packet.setPrivateValue("k", 0); // ?
        }


        return packet;
    }

    public Packet getAttachPacket(int line) {
        Packet packet;
        if (version.startsWith("v1_6")) {
            packet = new Packet("Packet39AttachEntity");
            packet.setPublicValue("a", 0);
            packet.setPublicValue("b", entityId + 2 * line);
            packet.setPublicValue("c", entityId + 1 + 2 * line);
        } else {
            packet = new Packet("PacketPlayOutAttachEntity");
            packet.setPrivateValue("a", 0);
            packet.setPrivateValue("b", entityId + 2 * line);
            packet.setPrivateValue("c", entityId + 1 + 2 * line);
        }


        return packet;
    }

    private Object getWatcher(String message) {
        try {
            Object watcher;
            if (version.startsWith("v1_6")) {
                watcher = ReflectionUtil.getNMSClass("DataWatcher");
            } else {
                watcher = ReflectionUtil.getNMSClass("DataWatcher", 1, null);
            }
            Method a = ReflectionUtil.getMethod("a", watcher.getClass(), new Class<?>[] {int.class, Object.class});
            a.setAccessible(true);
            a.invoke(watcher, 10, message);
            a.invoke(watcher, 11, Byte.valueOf((byte)1));
            a.invoke(watcher, 12, Integer.valueOf(-1700000));
            return watcher;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
/*
    private Packet getMovePacket(int line, int entityId, Location location) {
        Packet packet = new Packet("Packet34EntityTeleport");
        packet.setPublicValue("a", entityId);
        packet.setPublicValue("b", (int)Math.floor(location.getX() * 32D)); // X
        packet.setPublicValue("d", (int)Math.floor(((location.getY() - 0.25D * line) + 55D) * 32D)); // Y
        packet.setPublicValue("d", (int)Math.floor(location.getZ() * 32D)); // Z
        packet.setPublicValue("e", (byte)0); // Pitch
        packet.setPublicValue("f", (byte)0); // Yaw
        return packet;
    }*/

    private String formatMessage(String message, Player player) {
        message = message.replace("{player}", player.getName());
        message = message.replaceAll("`([a-z0-9])", "\u00A7$1");
        return message;
    }

    public boolean isAbsolute() {
        return absolute;
    }
}
